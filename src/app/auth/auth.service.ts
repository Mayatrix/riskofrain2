import { Injectable } from '@angular/core'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { catchError, tap } from 'rxjs/operators'
import { throwError, Subject, BehaviorSubject, Observable } from 'rxjs'
import { User } from './user.model'
import { Router } from '@angular/router'
import { environment } from '../../environments/environment'

export interface AuthResponseData {
  kind: string

  id: string
  email: string
  token: string
  createdDate: string
  registered?: boolean
  username: string
}

@Injectable({ providedIn: 'root' })
export class AuthService {

  BASEURL = environment.apiUrl
  // BASEURL = 'https://ror2api.herokuapp.com/api/users'

  USERSURL = '/users'
  REGISTERURL = '/register'
  AUTHENTICATEURL = '/authenticate'

  user = new BehaviorSubject<User>(null)

  constructor(private http: HttpClient, private router: Router) {}

  signup(email: string, password: string, username: string) {
    return this.http
      .post<AuthResponseData>(this.BASEURL + this.USERSURL + this.REGISTERURL, {
        email,
        username,
        password
      })
      .pipe(
        catchError(this.handleError),
        tap((resData) => {
          this.handleAuthentication(
            resData.email,
            resData.id,
            resData.token,
            resData.createdDate,
            resData.username
          )
        })
      )
  }

  login(email: string, password: string) {
    return this.http
      .post<AuthResponseData>(this.BASEURL + this.USERSURL + this.AUTHENTICATEURL, {
        email,
        password
      })
      .pipe(
        catchError(this.handleError),
        tap((resData) => {
          this.handleAuthentication(
            resData.email,
            resData.id,
            resData.token,
            resData.createdDate,
            resData.username
          )
        })
      )
  }

  getUserById(userId: string): Observable<User>{
    return this.http
    .get<User>(this.BASEURL + this.USERSURL + '/' + userId)
    .pipe(catchError(this.handleError))
  }

  autoLogin() {
    const userData: {
      email: string
      _token: string
      createdDate: string
      _id: string
      username: string
    } = JSON.parse(localStorage.getItem('userData'))

    if (!userData) {
      return
    }

    const loadedUser = new User(
      userData.email,
      userData._id,
      userData._token,
      userData.createdDate,
      userData.username
    )

    if (loadedUser.token) {
      this.user.next(loadedUser)
    }
  }

  logout() {
    localStorage.removeItem('userData')
    this.user.next(null)
    this.router.navigate(['/auth'])
  }

  private handleAuthentication(
    email: string,
    _id: string,
    token: string,
    createdDate: string,
    username: string
  ) {
    const user = new User(email, _id, token, createdDate, username)
    this.user.next(user)

    // Add to local storage
    localStorage.setItem('userData', JSON.stringify(user))
  }

  private handleError(errorRes: HttpErrorResponse) {
    const errorMessage = 'The credentials are incorrect.'

    return throwError(errorMessage)
  }
}
