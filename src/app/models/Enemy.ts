export class Enemy {
  _id: string
  name: string
  health: number
  damage: number
  userId
  weakness: string
}
