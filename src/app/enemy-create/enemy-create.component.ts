import { Component, OnInit } from '@angular/core'
import { AbstractControl, FormControl, FormGroup, NgForm, Validators } from '@angular/forms'
import { AuthService } from '../auth/auth.service'
import { EnemyService } from '../services/enemy.service'
import { Router, ActivatedRoute } from '@angular/router'
import { ValueConverter } from '@angular/compiler/src/render3/view/template'
import { Item } from '../models/Item'
import { Enemy } from '../models/Enemy'
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-enemy-create',
  templateUrl: './enemy-create.component.html',
  styleUrls: ['./enemy-create.component.scss']
})
export class EnemyCreateComponent implements OnInit {
  userId: string
  newEnemy = true
  enemyId: string
  headerTitle: string
  enemyIdToPost: string

  tierValidator(control: AbstractControl): { [key: string]: any } | null {
    var allowed = !/Green/.test(control.value)

    if (allowed) {
      allowed = !/Red/.test(control.value)
      if (allowed) {
        allowed = !/Blue/.test(control.value)
        if (allowed) {
          allowed = !/Orange/.test(control.value)
          if (allowed) {
            allowed = !/Yellow/.test(control.value)
          }
        }
      }
    }

    return allowed ? { forbiddenName: { value: control.value } } : null
  }

  myEpicForm = new FormGroup({
    enemyName: new FormControl('', [Validators.required, Validators.maxLength(40), Validators.minLength(1)]),
    enemyHealth: new FormControl('', [Validators.required, Validators.max(1000), Validators.min(1)]),
    enemyDamage: new FormControl('', [Validators.required, Validators.max(200), Validators.min(1)]),
    enemyWeakness: new FormControl('', [Validators.required, this.tierValidator])
  })

  tiers = ['Green', 'Red', 'Blue', 'Orange', 'Yellow']
  constructor
  (private authService: AuthService,
    private enemyService: EnemyService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    const enemyId = this.route.snapshot.paramMap.get('enemyId')

    var datePipe = new DatePipe("en-US");

    if(enemyId !== null){
      this.enemyIdToPost = enemyId
      this.newEnemy = false

      this.enemyService.getEnemy(enemyId).subscribe((enemy) => {

        this.myEpicForm.setValue({
          enemyName: enemy.name,
          enemyHealth: enemy.health,
          enemyDamage: enemy.damage,
          enemyWeakness: enemy.weakness
        })
      })
      this.headerTitle = 'Update Enemy'
    }
    else {
      this.headerTitle = 'Create Enemy'
    }
    this.authService.user.subscribe((user) => {
      this.userId = user === null ? '' : user._id
    })
  }

  onSaveEnemy(form: FormGroup){
    const value = form.value


    if(this.newEnemy){
      const enemyParams = {
        name: value.enemyName,
        weakness: value.enemyWeakness,
        damage: value.enemyDamage,
        health: value.enemyHealth,
        userId: this.userId
      }

      this.enemyService.createEnemy(enemyParams).subscribe((enemy) => {
        this.router.navigate(['/enemies'])
      })
    }
    else {
      const enemyParams = {
        name: value.enemyName,
        weakness: value.enemyWeakness,
        damage: value.enemyDamage,
        health: value.enemyHealth
      }

      this.enemyService.updateEnemy(this.enemyIdToPost, enemyParams).subscribe((enemy) => {
        this.router.navigate(['/enemies'])
      })
    }

  }

}
