import { Component, OnInit } from '@angular/core'
import { AbstractControl, FormControl, FormGroup, NgForm, Validators } from '@angular/forms'
import { AuthService } from '../auth/auth.service'
import { ItemService } from '../services/item.service'
import { Router, ActivatedRoute } from '@angular/router'
import { ValueConverter } from '@angular/compiler/src/render3/view/template'
import { Item } from '../models/Item'
import { CharacterService } from '../services/character.service'

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {
  userId: string
  characterId: string
  items: Item[]

  myEpicForm = new FormGroup({
    itemId: new FormControl('', [Validators.required]),
  })

  constructor(private authService: AuthService,
    private itemService: ItemService,
    private characterService: CharacterService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.itemService.getItems().subscribe(theitems => {
      this.items = theitems
    })

    this.characterId = this.route.snapshot.paramMap.get('characterId')

    this.authService.user.subscribe((user) => {
      this.userId = user === null ? '' : user._id
    })
  }

  onAddItem(characterId: string, itemId: string){
    this.characterService.addItemToCharacter(characterId, itemId).subscribe((item) => {
      this.router.navigate(['/characters'])
    })
  }

}
