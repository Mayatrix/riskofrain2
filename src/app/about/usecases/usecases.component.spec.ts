import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { UsecasesComponent } from './usecases.component'
import { UsecaseComponent } from './usecase/usecase.component'
import { SharedModule } from 'src/app/core/Shared/shared.module'

describe('UsecaseComponent', () => {
  let component: UsecasesComponent
  let fixture: ComponentFixture<UsecasesComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [UsecasesComponent, UsecaseComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(UsecasesComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
