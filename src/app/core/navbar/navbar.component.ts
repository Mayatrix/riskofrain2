import { Component, Input, OnDestroy, OnInit } from '@angular/core'
import { AuthService } from '../../auth/auth.service'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [
    '.btn-link { color: rgba(255,255,255,.5); text-decoration: none; }',
    // tslint:disable-next-line: max-line-length
    '.btn-link.focus, .btn-link:focus, .btn-link.hover, .btn-link:hover { color: rgba(255,255,255,.75); text-decoration: none; box-shadow: none; }'
  ]
})
export class NavbarComponent implements OnInit, OnDestroy {
  private userSubscription: Subscription
  public isAuthenticated = false
  @Input() title: string
  isNavbarCollapsed = true

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.userSubscription = this.authService.user.subscribe((user) => {
      this.isAuthenticated = !!user
    })
  }

  onLogout() {
    this.authService.logout()
  }

  ngOnDestroy() {
    if (this.userSubscription !== undefined) {
      this.userSubscription.unsubscribe()
    }
  }
}
