export class User {
  constructor(
    public email: string,
    public _id: string,
    private _token: string,
    public createdDate,
    public username: string
  ) {}

  get token() {
    return this._token
  }
}
