import { getLocaleDateTimeFormat } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Observable, of, throwError } from 'rxjs';
import { isDate } from 'util';
import { SharedModule } from '../core/Shared/shared.module';
import { Enemy } from '../models/Enemy';

import { EnemyService } from './enemy.service';

describe('EnemyService', () => {
  //De echte service die we gaan testen
  let enemyService: EnemyService;

  // Mock services die we aanmaken voor dependency injection in de constructor
  let httpSpy: any;
  let enemyServiceSpy: any;
  let authServiceSpy: any;

  let date = new Date

  const expectedEnemies: Enemy[] = [{
    _id: '12345',
    name: 'SomeEnemy',
    health: 100,
    damage: 100,
    userId: '123',
    weakness: 'Blue'
  },
  {
    _id: '12345',
    name: 'AnotherEnemy',
    health: 100,
    damage: 100,
    userId: '123',
    weakness: 'Red'
  }
]


  const expectedEnemy: Enemy = {
    _id: '12345',
    name: 'SomeEnemy',
    health: 100,
    damage: 100,
    userId: '123',
    weakness: 'Blue'
  }

  beforeEach(() => {
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

  authServiceSpy = jasmine.createSpyObj('AuthService', [
    'signup',
    'login',
    'autoLogin',
    'logout',
    'handleAuthentication'
  ]);

  enemyService = new EnemyService(httpSpy, authServiceSpy)


  enemyServiceSpy = jasmine.createSpyObj('EnemyService', ['getEnemy', 'getEnemiesByUserId', 'getEnemies', 'createEnemy', 'updateEnemy', 'deleteEnemy', 'handleError'])
  });

  it('should be created', () => {
    expect(enemyService).toBeTruthy();
  });

  it('should retrieve one enemy', () => {
    httpSpy.get.and.returnValue(of(expectedEnemy))
    enemyServiceSpy.getEnemy.and.returnValue(of(expectedEnemy))

    const subs = enemyService.getEnemy(expectedEnemy._id).subscribe((enemy) => {
      expect(enemy).toEqual(expectedEnemy)
    })

    subs.unsubscribe()
  })

  it('should NOT retrieve a enemy, if enemeyId doesnt exist', () => {

    const expectedErrorResponse = {
      error: { message: 'Enemy was not found' },
      name: 'HttpErrorResponse',
      ok: false,
      status: 422,
      statusText: 'Unauthorized',
    };


    httpSpy.get.and.returnValue(throwError(expectedErrorResponse))

 enemyService.getEnemy('12412').subscribe((enemy) => {
      expect(enemy).toBeUndefined
    }, (error) => {
      expect(error).toEqual(expectedErrorResponse)
    })
  })

  it('should retrieve all enemies', () => {
    httpSpy.get.and.returnValue(of(expectedEnemies))
    enemyServiceSpy.getEnemies.and.returnValue(of(expectedEnemy))

    const subs = enemyService.getEnemies().subscribe((enemies) => {
      expect(enemies).toEqual(expectedEnemies)
    })

    subs.unsubscribe()
  })

  it('should retrieve all enemies by userId', () => {
    httpSpy.get.and.returnValue(of(expectedEnemies))
    enemyServiceSpy.getEnemiesByUserId.and.returnValue(of(expectedEnemies))

    enemyService.getEnemiesByUserId('123').subscribe((enemies) => {
      expect(enemies).toEqual(expectedEnemies)
    })
  })

  it('should not retrieve all enemies by userId if userId doesnt exist', () => {

    const expectedErrorResponse = {
      error: { message: 'Enemies were not found' },
      name: 'HttpErrorResponse',
      ok: false,
      status: 422,
      statusText: 'Unauthorized',
    };

    httpSpy.get.and.returnValue(throwError(expectedErrorResponse))

    enemyService.getEnemiesByUserId('123').subscribe((enemies) => {
      expect(enemies).toBeUndefined()
    }, error => {
      expect(error).toEqual(expectedErrorResponse)
    })
  })

  it('should be able to update a enemy by its Id', () => {

    const expectedUpdatedEnemy: Enemy = {
      _id: '12345',
      name: 'SomeDifferentName',
      health: 100,
      damage: 100,
      userId: '123',
      weakness: 'Blue',
    }

    const _id = '12345'
    const name = 'SomeDifferentName'
    const health = 100
    const damage = 100
    const userId = '123'
    const weakness = 'Blue'

    const enemyParams = {
      name,
      health,
      damage,
      userId,
      weakness,
    }

    httpSpy.put.and.returnValue(of(expectedUpdatedEnemy))
    enemyServiceSpy.updateEnemy.and.returnValue(of(expectedUpdatedEnemy))

    enemyService.updateEnemy(_id, enemyParams).subscribe((enemy) => {
      expect(enemy).toEqual(expectedUpdatedEnemy)
    })
  })

  it('should should fail to update a enemy by its Id if parameters are wrong', () => {

    const expectedErrorResponse = {
      error: { message: 'Enemy was not found' },
      name: 'HttpErrorResponse',
      ok: false,
      status: 422,
      statusText: 'Unauthorized',
    };


    httpSpy.put.and.returnValue(throwError(expectedErrorResponse))

    const expectedUpdatedEnemy: Enemy = {
      _id: '12345',
      name: 'SomeDifferentName',
      health: 100,
      damage: 100,
      userId: '123',
      weakness: 'Blue',
    }

    const _id = '12345'
    const name = 'SomeDifferentName'
    const health = 'astring'
    const damage = 100
    const userId = '123'
    const weakness = 'Blue'

    const enemyParams = {
      name,
      health,
      damage,
      userId,
      weakness,
    }

    enemyService.updateEnemy(_id, enemyParams).subscribe((enemy) => {
      expect(enemy).toBeUndefined
    }, error => {
      expect(error).toEqual(expectedErrorResponse)
    })
  })

  it('should be able to delete a enemy by enemyId', () => {
    httpSpy.delete.and.returnValue(of(expectedEnemy))
    enemyServiceSpy.deleteEnemy.and.returnValue(of(expectedEnemy))

    enemyService.deleteEnemy('12345').subscribe((enemy) => {
      expect(enemy).toEqual(expectedEnemy)
    })
  })

  it('should not be able to delete a enemy by enemyId if the enemyId is wrong', () => {

    const expectedErrorResponse = {
      error: { message: 'Enemy was not found' },
      name: 'HttpErrorResponse',
      ok: false,
      status: 422,
      statusText: 'Unauthorized',
    };

    httpSpy.delete.and.returnValue(throwError(expectedErrorResponse))

    enemyService.deleteEnemy('12345').subscribe((enemy) => {
      expect(enemy).toBeUndefined
    }, error => {
      expect(error).toEqual(expectedErrorResponse)
    })
  })

  it('should be able to create a enemy', () => {

    const name = 'SomeEnemy'
    const health = 100
    const damage = 100
    const userId = '123'
    const weakness = 'Blue'

    const enemyParams = {
      name,
      health,
      damage,
      userId,
      weakness
    }

    httpSpy.post.and.returnValue(of(expectedEnemy))
    enemyServiceSpy.createEnemy.and.returnValue(of(expectedEnemy))

    enemyService.createEnemy(enemyParams).subscribe((enemy) => {
      expect(enemy).toEqual(expectedEnemy)
    })
  })

  it('should not be able to create a enemy with wrong parameters', () => {

    const expectedErrorResponse = {
      error: { message: 'Enemy can not be created' },
      name: 'HttpErrorResponse',
      ok: false,
      status: 422,
      statusText: 'Unauthorized',
    };

    const name = true
    const health = 5123
    const damage = 12552
    const userId = '123'
    const weakness = 'Blue'

    const enemyParams = {
      name,
      health,
      damage,
      userId,
      weakness
    }

    httpSpy.post.and.returnValue(throwError(expectedErrorResponse))

    enemyService.createEnemy(enemyParams).subscribe((enemy) => {
      expect(enemy).toBeUndefined()
    }, error => {
      expect(error).toEqual(expectedErrorResponse)
    })
  })
});
