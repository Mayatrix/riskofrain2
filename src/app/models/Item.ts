export class Item {
  _id: string
  name: string
  description: string
  userId
  tier: string
  amount: number
}
