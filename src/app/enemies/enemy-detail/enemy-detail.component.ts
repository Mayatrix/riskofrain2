import { Component, OnInit } from '@angular/core'
import { NgForm } from '@angular/forms'
import { AuthService } from '../../auth/auth.service'
import { Router, ActivatedRoute } from '@angular/router'
import { ValueConverter } from '@angular/compiler/src/render3/view/template'
import { EnemyService } from 'src/app/services/enemy.service'
import { Enemy } from 'src/app/models/Enemy'

@Component({
  selector: 'app-enemy-detail',
  templateUrl: './enemy-detail.component.html',
  styleUrls: ['./enemy-detail.component.scss']
})
export class EnemyDetailComponent implements OnInit {
  enemy: Enemy

  constructor(
    private authService: AuthService,
    private enemyService: EnemyService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    const enemyId = this.route.snapshot.paramMap.get('enemyId')

    if (enemyId !== null) {
      this.enemyService.getEnemy(enemyId).subscribe((enemy) => {
        this.enemy = enemy
      })
    }
  }

  onBack() {
    this.router.navigate(['/enemies'])
  }

}
