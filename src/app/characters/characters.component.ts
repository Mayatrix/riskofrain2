import { Component, OnInit } from '@angular/core'
import { Character } from '../models/Character'
import { CharacterService } from '../services/character.service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { Observable, Subscription } from 'rxjs'
import { interval } from 'rxjs'
import { mapTo, startWith, map, flatMap } from 'rxjs/operators'
import { AuthService } from '../auth/auth.service'
import { Ability } from '../models/Ability'

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {
  characters: Observable<Character[]>
  refreshrate = 4500
  isLoading = true
  public isAuthenticated = false
  private userSubscription: Subscription

  constructor(
    private characterService: CharacterService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.characters = interval(this.refreshrate).pipe(
      startWith(0),
      mapTo((this.characters = this.characterService.getCharacters())),
      flatMap((v) => v)
    )

    if (this.characters !== undefined) {
      this.characters.subscribe(() => {
        this.isLoading = false
      })
    }

    {
      this.userSubscription = this.authService.user.subscribe((user) => {
        this.isAuthenticated = !!user
      })
    }
  }
}
