import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { SharedModule } from '../core/Shared/shared.module';
import { ItemService } from '../services/item.service';

import { AddItemComponent } from './add-item.component';

describe('AddItemComponent', () => {
  let component: AddItemComponent;
  let fixture: ComponentFixture<AddItemComponent>;
  let itemServiceSpy: any;

  beforeEach(async () => {

    itemServiceSpy = jasmine.createSpyObj('ItemService', [
      'getItem',
      'getItemsByUserId',
      'getItems',
      'createItem',
      'updateItem',
      'deleteITem',
      'handleError'
    ]);

    itemServiceSpy.getItems.and.returnValue(new Observable<any>());

    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [ AddItemComponent ],
      providers: [{provide: ItemService, useValue: itemServiceSpy}]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
