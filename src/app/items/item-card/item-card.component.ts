import { Component, Input, OnInit } from '@angular/core'
import { Item } from '../../models/Item'
import { AuthService } from '../../auth/auth.service'
import { ItemService } from '../../services/item.service'
import { Router } from '@angular/router'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss']
})
export class ItemCardComponent implements OnInit {
  @Input() item: Item
  userId: string
  userEmail: string
  destroyed = false
  public isAuthenticated = false
  private userSubscription: Subscription

  constructor(private authService: AuthService, private itemservice: ItemService, private router: Router) {}

  ngOnInit() {
    this.authService.user.subscribe((user) => {
      this.userId = user === null ? '' : user._id
      this.userEmail = user === null ? '' : user.email
    })
    {
      this.userSubscription = this.authService.user.subscribe((user) => {
        this.isAuthenticated = !!user
      })
    }
  }
  onDelete(itemId: string) {
    this.destroyed = true
    this.itemservice.deleteITem(itemId).subscribe((character) => {})
  }

  onEdit(itemId: string) {
    this.router.navigate(['/createitem', { itemId }])
  }

  onDetails(itemId: string) {
    this.router.navigate(['/itemdetails', { itemId }])
  }

}
