import { Component, OnInit } from '@angular/core'
import { Enemy } from '../models/Enemy'
import { EnemyService } from '../services/enemy.service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { Observable, Subscription } from 'rxjs'
import { interval } from 'rxjs'
import { mapTo, startWith, map, flatMap } from 'rxjs/operators'
import { AuthService } from '../auth/auth.service'

@Component({
  selector: 'app-enemies',
  templateUrl: './enemies.component.html',
  styleUrls: ['./enemies.component.scss']
})
export class EnemiesComponent implements OnInit {
  enemies: Observable<Enemy[]>
  refreshInterval = 4500
  isLoading = true
  public isAuthenticated = false
  private userSubscription: Subscription


  constructor(private enemySerivce: EnemyService, private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.enemies = interval(this.refreshInterval).pipe(
      startWith(0),
      mapTo((this.enemies = this.enemySerivce.getEnemies())),
      flatMap((v) => v)
    )

    if (this.enemies !== undefined) {
      this.enemies.subscribe(() => {
        this.isLoading = false
      })
    }

    {
      this.userSubscription = this.authService.user.subscribe((user) => {
        this.isAuthenticated = !!user
      })
    }
  }
}
