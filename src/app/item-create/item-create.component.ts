import { Component, OnInit } from '@angular/core'
import { AbstractControl, FormControl, FormGroup, NgForm, Validators } from '@angular/forms'
import { AuthService } from '../auth/auth.service'
import { ItemService } from '../services/item.service'
import { Router, ActivatedRoute } from '@angular/router'
import { ValueConverter } from '@angular/compiler/src/render3/view/template'
import { Item } from '../models/Item'
import { CharacterService } from '../services/character.service'

@Component({
  selector: 'app-item-create',
  templateUrl: './item-create.component.html',
  styleUrls: ['./item-create.component.scss']
})
export class ItemCreateComponent implements OnInit {
  userId: string
  newItem = true
  characterIdToPost: string

  formName: string
  formDescription: string
  formTier: string
  headerTitle: string
  itemId: string
  characterId: string
  postToCharacter = false

  tierValidator(control: AbstractControl): { [key: string]: any } | null {
    var allowed = !/Green/.test(control.value)

    if (allowed) {
      allowed = !/Red/.test(control.value)
      if (allowed) {
        allowed = !/Blue/.test(control.value)
        if (allowed) {
          allowed = !/Orange/.test(control.value)
          if (allowed) {
            allowed = !/Yellow/.test(control.value)
          }
        }
      }
    }

    return allowed ? { forbiddenName: { value: control.value } } : null
  }

  myEpicForm = new FormGroup({
    itemName: new FormControl('', [Validators.required, Validators.maxLength(40), Validators.minLength(1)]),
    itemDescription: new FormControl('', [Validators.required, Validators.minLength(1)]),
    itemTier: new FormControl('', [Validators.required, this.tierValidator])
  })

  tiers = ['Green', 'Red', 'Blue', 'Orange', 'Yellow']
  constructor(
    private authService: AuthService,
    private itemService: ItemService,
    private characterService: CharacterService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const itemId = this.route.snapshot.paramMap.get('itemId')
    const characterId = this.route.snapshot.paramMap.get('characterId')
    console.log(characterId)

    if(characterId !== null){
      this.characterIdToPost = characterId
      this.postToCharacter = true
    }

    if (itemId !== null) {
      this.newItem = false

      this.itemService.getItem(itemId).subscribe((item) => {
        this.formName = item.name
        this.formDescription = item.description
        this.formTier = item.tier
        this.itemId = itemId

        this.myEpicForm.setValue({
          itemName: item.name,
          itemDescription: item.description,
          itemTier: item.tier
        })
      })
      this.headerTitle = 'Update Item'
    } else {
      this.headerTitle = 'Create Item'
    }

    this.authService.user.subscribe((user) => {
      this.userId = user === null ? '' : user._id
    })
  }

  onSaveitem(form: FormGroup) {
    const value = form.value

    if (this.newItem) {
      const itemParams = {
        name: value.itemName,
        description: value.itemDescription,
        tier: value.itemTier,
        userId: this.userId
      }

      console.log(this.characterId)

      if(this.postToCharacter){
        this.itemService.createItem(itemParams).subscribe((item) => {
          this.characterService.addItemToCharacter(this.characterIdToPost, item._id).subscribe((character) => {
            this.router.navigate(['/characters'])
          })
        })
      }

      else {
        this.itemService.createItem(itemParams).subscribe((item) => {
          this.router.navigate(['/items'])
        })
      }
    } else {
      const itemParams = {
        name: value.itemName,
        description: value.itemDescription,
        tier: value.itemTier
      }

      this.itemService.updateItem(this.itemId, itemParams).subscribe((item) => {
        this.router.navigate(['/items'])
      })
    }
  }
}
