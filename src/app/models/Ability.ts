import { StringDecoder } from 'string_decoder'

export class Ability {
  _id: string
  name: string
  description: string
  userId
  damage: number
  cooldown: number
}
