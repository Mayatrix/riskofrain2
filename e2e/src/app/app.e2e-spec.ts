import { AppPage } from './app.po'
import { browser, logging, element, by, ElementArrayFinder } from 'protractor'
import { protractor } from 'protractor/built/ptor';

describe('Home Page', () => {
  let page: AppPage

  beforeEach(() => {
    page = new AppPage()
    page.navigateTo()
  })

  it('should display "Risk of Rain 2" as navbar-brand', () => {
    expect(page.getTitleText()).toEqual('')
  })

  it('should display "Welkom op het informatiesysteem voor Risk of Rain 2" as navbar-brand', () => {
    expect(page.pageWelcomMessage.getText()).toContain('Welkom op het informatiesysteem voor Risk of Rain 2')
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE
      } as logging.Entry)
    )
  })
})

describe('Login Page', () => {
  let page: AppPage

  beforeEach(() => {
    page = new AppPage()
    page.navigateTo('/auth')
  })

  it('Should be able to log in when input fields are filled in correctly', () => {
    browser.waitForAngularEnabled(false);

    page.getEmailInputField.sendKeys('test@test.nl');
    page.getPasswordInputField.sendKeys('123456');
    expect(page.getSubmitButton.isEnabled()).toBe(true);

    page.getSubmitButton.click();

    browser.driver.sleep(500);
    expect(browser.getCurrentUrl()).toContain('/characters');

    // tslint:disable-next-line: quotemark
    const getStoredUser = "return window.localStorage.getItem('userData');";
    browser.executeScript(getStoredUser).then((user) => {
      expect(user).toBeTruthy();
      expect(user).toContain('test@test.nl');
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE
      } as logging.Entry)
    )

    browser.executeScript("window.localStorage.removeItem('userData')");
  })
})

describe('(Logged In) Characters Page', () => {
  let page: AppPage

  beforeEach(() => {
    page = new AppPage()
    page.navigateTo('/auth')
    page.getEmailInputField.sendKeys('test@test.nl');
    page.getPasswordInputField.sendKeys('123456');

    page.getSubmitButton.click();

    browser.driver.sleep(500);
  })

  it('Should be at characters page after logging in, and since the user is logged in should be able to see a "Create Character" button', () => {
    browser.waitForAngularEnabled(false)
    expect(browser.getCurrentUrl()).toContain('/characters')
    expect(page.getCreateCharacterButton.isDisplayed()).toBe(true)
  })

  it('Shouldnt be able to see the charactercreate button after logging out', () => {
    browser.waitForAngularEnabled(false)
    expect(browser.getCurrentUrl()).toContain('/characters')
    expect(page.getCreateCharacterButton.isDisplayed()).toBe(true)
    page.getLogÖutButton.click()
    page.navigateTo('/characters')
    expect(page.getCreateCharacterButton.isPresent()).toBe(false)
  })

  it('Character create page should show error messages to the User when fields are not filled in (correctly)', () => {
    browser.waitForAngularEnabled(false)
    page.getCreateCharacterButton.click();
    expect(browser.getCurrentUrl()).toContain('/createcharacter')

    page.getCharacterNameInputField.click()
    page.getCharacterHealthInputField.click()
    page.getCharacterSpeedInputField.click()
    page.getCharacterDamageInputField.click()

    page.getCharacterNameInputField.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    page.getCharacterNameInputField.sendKeys(protractor.Key.BACK_SPACE);

    page.getCharacterHealthInputField.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'b'));
    page.getCharacterHealthInputField.sendKeys(protractor.Key.BACK_SPACE);

    page.getCharacterDamageInputField.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'c'));
    page.getCharacterDamageInputField.sendKeys(protractor.Key.BACK_SPACE);

    page.getCharacterSpeedInputField.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'd'));
    page.getCharacterSpeedInputField.sendKeys(protractor.Key.BACK_SPACE);


    browser.driver.sleep(100);

    expect(page.characterNameInvalid).toBeTruthy();
    expect(page.characterHealthInvalid).toBeTruthy();
    expect(page.characterSpeedInvalid).toBeTruthy();
    expect(page.characterDamageInvalid).toBeTruthy();

    expect(page.characterSubmitButton.isEnabled()).toBe(false);


  })

  it('A logged in User should be able to create a character', () => {
    browser.waitForAngularEnabled(false)
    page.getCreateCharacterButton.click();
    expect(browser.getCurrentUrl()).toContain('/createcharacter')

    page.getCharacterNameInputField.sendKeys('test');
    page.getCharacterHealthInputField.sendKeys(1);
    page.getCharacterDamageInputField.sendKeys(2);
    page.getCharacterSpeedInputField.sendKeys(1);
    expect(page.characterSubmitButton.isEnabled()).toBe(true);

    page.characterSubmitButton.click();
    expect(browser.getCurrentUrl()).toContain('/characters')


    browser.driver.sleep(100);
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE
      } as logging.Entry)
    )
    browser.executeScript("window.localStorage.removeItem('userData')");
  })
})
