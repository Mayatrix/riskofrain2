import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { UsecaseComponent } from './about/usecases/usecase/usecase.component'
import { RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { CharactersComponent } from './characters/characters.component'
import { ItemsComponent } from './items/items.component'
import { AbilitiesComponent } from './abilities/abilities.component'
import { AuthComponent } from './auth/auth.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { AuthInterceptorService } from './auth/auth-interceptor.service'
import { CharacterService } from './services/character.service'
import { CharacterCardComponent } from './characters/character-card/character-card.component'
import { SharedModule } from './core/Shared/shared.module'
import { HomeComponent } from './home/home.component'
import { CharacterCreateComponent } from './character-create/character-create.component'
import { ItemCreateComponent } from './item-create/item-create.component'
import { ItemCardComponent } from './items/item-card/item-card.component'
import { CharacterDetailComponent } from './characters/character-detail/character-detail.component'
import { AbilityCreateComponent } from './ability-create/ability-create.component';
import { AddAbilityComponent } from './add-ability/add-ability.component';
import { AddItemComponent } from './add-item/add-item.component';
import { EnemiesComponent } from './enemies/enemies.component';
import { EnemyCardComponent } from './enemies/enemy-card/enemy-card.component';
import { EnemyCreateComponent } from './enemy-create/enemy-create.component';
import { EnemyDetailComponent } from './enemies/enemy-detail/enemy-detail.component';
import { ItemDetailComponent } from './items/item-detail/item-detail.component';
import { UserdetailsComponent } from './userdetails/userdetails.component'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UsecasesComponent,
    UsecaseComponent,
    DashboardComponent,
    CharactersComponent,
    ItemsComponent,
    AbilitiesComponent,
    AuthComponent,
    CharacterCardComponent,
    HomeComponent,
    CharacterCreateComponent,
    ItemCreateComponent,
    ItemCardComponent,
    CharacterDetailComponent,
    AbilityCreateComponent,
    AddAbilityComponent,
    AddItemComponent,
    EnemiesComponent,
    EnemyCardComponent,
    EnemyCreateComponent,
    EnemyDetailComponent,
    ItemDetailComponent,
    UserdetailsComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [
    CharacterService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
