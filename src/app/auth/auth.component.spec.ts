import { TestBed, async, ComponentFixture } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { AuthComponent } from './auth.component'
import { UsecasesComponent } from '../about/usecases/usecases.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { UsecaseComponent } from 'src/app/about/usecases/usecase/usecase.component'
import { DashboardComponent } from '../core/dashboard/dashboard.component'
import { NavbarComponent } from '../core/navbar/navbar.component'

import { SharedModule } from '../../app/core/Shared/shared.module'
import { AuthService } from './auth.service'

describe('AuthComponent', () => {
  let fixture: ComponentFixture<AuthComponent>
  let component: AuthComponent
  let authService: AuthService

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, NgbModule, SharedModule],
      declarations: [AuthComponent, DashboardComponent, NavbarComponent, UsecasesComponent, UsecaseComponent],
      providers: []
    }).compileComponents()

    fixture = TestBed.createComponent(AuthComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  }))

  it('should create ', () => {
    expect(component).toBeTruthy()
  })
})
