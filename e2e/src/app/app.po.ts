import { by, element, ElementFinder } from 'protractor'
import { CommonPageObject } from '../common.po'

export class AppPage extends CommonPageObject {

  //Home Page

  getTitleText() {
    return element(by.css('app-root .navbar-brand')).getText() as Promise<string>
  }

  get pageWelcomMessage() {
    return element(by.id('welcomeMessage')) as ElementFinder
  }

  //Login Page

  get getEmailInputField() {
    return element(by.id('email')) as ElementFinder
  }

  get getPasswordInputField() {
    return element(by.id('password')) as ElementFinder
  }

  get getSubmitButton() {
    return element(by.id('submitbutton')) as ElementFinder
  }

  get getLogÖutButton() {
    return element(by.id('logoutbutton')) as ElementFinder
  }

  //Characters Page

  get getCreateCharacterButton() {
    return element(by.id('createCharacterButton')) as ElementFinder
  }

  get getCharacterNameInputField() {
    return element(by.id('characterName')) as ElementFinder
  }

  get getCharacterDamageInputField() {
    return element(by.id('characterDamage')) as ElementFinder
  }

  get getCharacterHealthInputField() {
    return element(by.id('characterHealth')) as ElementFinder
  }

  get getCharacterSpeedInputField() {
    return element(by.id('characterSpeed')) as ElementFinder
  }

  get characterNameInvalid() {
    return element(by.id('invalidName')) as ElementFinder
  }

  get characterSpeedInvalid() {
    return element(by.id('invalidspeed')) as ElementFinder
  }

  get characterDamageInvalid() {
    return element(by.id('invalidDamage')) as ElementFinder
  }

  get characterHealthInvalid() {
    return element(by.id('invalidhealth')) as ElementFinder
  }

  get characterSubmitButton() {
    return element(by.id('characterSubmitButton'))
  }

  //Ability Create Page

  get abilityNameInput() {
    return element(by.id('abilityNameInput')) as ElementFinder
  }

  get abilityDescriptionInput() {
    return element(by.id('abilityDescriptionInput')) as ElementFinder
  }

  get abilityDamageInput() {
    return element(by.id('abilityDamageInput')) as ElementFinder
  }

  get abilityCooldownInput() {
    return element(by.id('abilityCooldownInput')) as ElementFinder
  }


  get nameInvalid() {
    return element(by.id('nameInvalid')) as ElementFinder
  }

  get descriptionInvalid() {
    return element(by.id('descriptionInvalid')) as ElementFinder
  }

  get damageInvalid() {
    return element(by.id('damageInvalid')) as ElementFinder
  }

  get cooldownInvalid() {
    return element(by.id('cooldownInvalid')) as ElementFinder
  }
}
