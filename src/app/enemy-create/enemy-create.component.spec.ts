import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from '../core/Shared/shared.module';

import { EnemyCreateComponent } from './enemy-create.component';

describe('EnemyCreateComponent', () => {
  let component: EnemyCreateComponent;
  let fixture: ComponentFixture<EnemyCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [ EnemyCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnemyCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
