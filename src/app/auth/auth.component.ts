import { Component, OnInit, ViewChild } from '@angular/core'
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms'
import { AuthService, AuthResponseData } from './auth.service'
import { Observable } from 'rxjs'
import { Router } from '@angular/router'

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) {}

  username = null;
  email = null;
  password = null;
  @ViewChild('f')
  form: NgForm

  isLoginMode = true
  isLoading = false
  error: string = null

  // myEpicForm = new FormGroup({
  //   email: new FormControl('', [Validators.required, Validators.email]),
  //   username: new FormControl('', [Validators.required, Validators.max(20), Validators.min(5)]),
  //   password: new FormControl('', [Validators.required, Validators.max(20), Validators.min(6)])
  // })

  onSwitch() {
    this.isLoginMode = !this.isLoginMode
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return
    }

    const email = form.value.email
    const password = form.value.password
    const username = form.value.username

    let authObs: Observable<AuthResponseData>

    this.isLoading = true

    if (this.isLoginMode) {
      authObs = this.authService.login(email, password)
    } else {
      authObs = this.authService.signup(email, password, username)
    }

    authObs.subscribe(
      (resData) => {
        this.isLoading = false
        this.router.navigate(['characters'])
      },
      (errorMessage) => {
        this.error = errorMessage
        this.isLoading = false
      }
    )

    form.reset()
  }

  ngOnInit(){

  }
}
