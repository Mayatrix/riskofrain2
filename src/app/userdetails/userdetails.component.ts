import { Component, OnInit } from '@angular/core'
import { Item } from '../models/Item'
import { ItemService } from '../services/item.service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { Observable, Subscription } from 'rxjs'
import { interval } from 'rxjs'
import { mapTo, startWith, map, flatMap } from 'rxjs/operators'
import { AuthService } from '../auth/auth.service'
import { Ability } from '../models/Ability'
import { Enemy } from '../models/Enemy'
import { Character } from '../models/Character'
import { EnemyService } from '../services/enemy.service'
import { CharacterService } from '../services/character.service'
import { User } from '../auth/user.model'

@Component({
  selector: 'app-userdetails',
  templateUrl: './userdetails.component.html',
  styleUrls: ['./userdetails.component.scss']
})
export class UserdetailsComponent implements OnInit {
  userId: string
  username
  createdDate
  email
  items: Observable<Item[]>
  enemies: Observable<Enemy[]>
  characters: Observable<Character[]>
  refreshInterval = 4500
  isLoading = true
  public isAuthenticated = false
  private userSubscription: Subscription

  constructor(private itemService: ItemService, private enemyService: EnemyService,
     private characterService: CharacterService,private authService: AuthService,
     private router: Router) { }

  ngOnInit(): void {
    this.authService.user.subscribe((user) => {
      this.userId = user === null ? '' : user._id
      this.username = user === null ? '' : user.username
      this.createdDate = user === null ? '' : user.createdDate
      this.email = user === null ? '' : user.email
    })

    {
    this.items = interval(this.refreshInterval).pipe(
      startWith(0),
      mapTo((this.items = this.itemService.getItemsByUserId(this.userId))),
      flatMap((v) => v)
    )}

    {
      this.enemies = interval(this.refreshInterval).pipe(
        startWith(0),
        mapTo((this.enemies = this.enemyService.getEnemiesByUserId(this.userId))),
        flatMap((v) => v)
      )
    }

    {
        this.characters = interval(this.refreshInterval).pipe(
          startWith(0),
          mapTo((this.characters = this.characterService.getCharactersByUserId(this.userId))),
          flatMap((v) => v)
        )
    }
  }
}
