import { ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { SharedModule } from '../core/Shared/shared.module'
import { ReactiveFormsModule, FormsModule, NgForm, MaxLengthValidator, AbstractControl } from '@angular/forms'

import { ItemCreateComponent } from './item-create.component'

const thisName = 'itemName'
const thisDescription = 'itemDescription'
const thisTier = 'itemTier'

describe('ItemCreateComponent', () => {
  let component: ItemCreateComponent
  let fixture: ComponentFixture<ItemCreateComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [ItemCreateComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCreateComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('the form should be false when not filled in', () => {
    expect(component.myEpicForm.valid).toBeFalsy()
  })

  it('a category should be false when not filled in', () => {
    const name = component.myEpicForm.controls[thisName]
    expect(name.valid).toBeFalsy()
  })

  it('Name should be false if exceeds 40 characters', () => {
    const form = component.myEpicForm
    const name: AbstractControl = form.get(thisName)

    name.setValue('aReallyReallyReallyReallyReallyReallyReallyReallyReallyREallyLongName')
    expect(name.hasError('maxlength')).toBeTruthy()
    expect(form.valid).toBeFalsy()
  })

  it('Tier should be false if it is not one of the set Values', () => {
    const form = component.myEpicForm
    const tier: AbstractControl = form.get(thisTier)

    tier.setValue('White')
    expect(tier.valid).toBeFalsy()
  })

  it('the form should return true for a category if that category is filled in', () => {
    const name = component.myEpicForm.controls[thisName]
    name.setValue('anyName')
    expect(name.valid).toBeTruthy()
  })

  it('the form should return false if the form is not completely filled in', () => {
    const name = component.myEpicForm.controls[thisName]
    name.setValue('anyName')
    expect(component.myEpicForm.valid).toBeFalsy()
  })

  it('the form should return true if the form is completely filled in', () => {
    const name = component.myEpicForm.controls[thisName]
    const tier = component.myEpicForm.controls[thisTier]
    const description = component.myEpicForm.controls[thisDescription]
    name.setValue('anyName')
    tier.setValue('Green')
    description.setValue('anyDescription')
    expect(component.myEpicForm.valid).toBeTruthy()
  })
})
