import { ComponentFixture, TestBed } from '@angular/core/testing'
import { SharedModule } from '../core/Shared/shared.module'
import { ItemService } from 'src/app/services/item.service'
import { ItemsComponent } from './items.component'

describe('ItemsComponent', () => {
  let itemService: ItemService

  let httpSpy: any;
  let authServiceSpy: any;
  let itemServiceSpy: any;

  let component: ItemsComponent
  let fixture: ComponentFixture<ItemsComponent>

  beforeEach(async () => {

    const Items = []

    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);

    itemServiceSpy = jasmine.createSpyObj('ItemService', [
      'getItem',
      'getItemsByUserId',
      'getItems',
      'createItem',
      'updateItem',
      'deleteITem',
      'handleError'
    ]);

    itemServiceSpy.getItems.and.returnValue([]);


    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'signup',
      'login',
      'autoLogin',
      'logout',
      'handleAuthentication'
    ]);

    itemService = new  ItemService(httpSpy, authServiceSpy)

    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [ItemsComponent],
      providers: [{provide: ItemService, useValue: itemServiceSpy}]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
