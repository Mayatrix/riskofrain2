//
// This server will be started by Protractor in end-to-end tests.
// Add your API mocks for your specific project in this file.
//

const express = require('express')
const http = require('http')
const port = 4000

let app = express()
let routes = require('express').Router()

// Add CORS headers so our external Angular app is allowed to connect
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,authorization')
  res.setHeader('Access-Control-Allow-Credentials', true)
  next()
})

routes.post('/api/users/authenticate', (req, res, next) => {
  res.status(200).json({
    email: "test@test.nl",
    username: "testuser",
    createdDate: "2020-12-08T19:21:40.551Z",
    id: "123456",
    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1ZmNmZDI0NGQ5NzczOTMyZWM5NDVhMjEiLCJpYXQiOjE2MTgwNzM5MzcsImV4cCI6MTYxODY3ODczN30.qiPaBTUiHzCtVOw9ixvXPiwouNSVRiImTjLLoDkiFZI"
  })
})

routes.post('/api/characters/create', (req, res, next) => {
  res.status(200).json(
    {
      items: [],
      _id: "6072001a2ca6f12ce4e9679d",
      name: "test",
      health: 100,
      userId: "123456",
      speed: 50,
      basedamage: 500,
      abilities: [],
      __v: 0
    }
  )
})

routes.get('/api/characters', (req, res, next) => {
  res.status(200).json(
   [
  {
    items: [],
    _id: "5fcfd28bd9773932ec945a24",
    name: "Commando",
    health: 300,
    basedamage: 120,
    speed: 1.7,
    userId: {
      username: "roguelikenthousiast",
      id: "5fcfd244d9773932ec945a21"
    },
    abilities: [
      {
        _id: "5fd0056d2737f4149c34dc93",
        name: "Double guns",
        description: "Shoot with two guns at once",
        userId: "5fcfd244d9773932ec945a21",
        damage: 100,
        cooldown: 50
      },
      {
        _id: "5fd005842737f4149c34dc94",
        name: "Armageddon",
        description: "Release your ultimate power",
        userId: "5fcfd244d9773932ec945a21",
        damage: 223,
        cooldown: 50
      },
      {
        _id: "5fd005de2737f4149c34dc96",
        name: "Firebreath",
        description: "Release a deadly Firebreath",
        userId: "5fcfd244d9773932ec945a21",
        damage: 100,
        cooldown: 50
      }
    ],
    __v: 0
  },
  {
    items: [],
    _id: "5fcfd294d9773932ec945a25",
    name: "Rex",
    health: 200,
    basedamage: 95,
    speed: 2.3,
    userId: {
      username: "roguelikenthousiast",
      id: "5fcfd244d9773932ec945a21"
    },
    abilities: [],
    __v: 0
  },
  {
    items: [],
    _id: "5fcfd404d9773932ec945a2d",
    name: "The Captain",
    health: 51251252151,
    basedamage: 12521512,
    speed: 2512512512,
    userId: {
      username: "xXx_NoScoper1337_xXx",
      id: "5fcfd264d9773932ec945a22"
    },
    abilities: [],
    __v: 0
  },
  {
    items: [],
    _id: "5fcfd41bd9773932ec945a2e",
    name: "Engineer",
    health: 25235235,
    basedamage: 3245657,
    speed: 325325325,
    userId: {
      username: "xXx_NoScoper1337_xXx",
      id: "5fcfd264d9773932ec945a22"
    },
    abilities: [],
    __v: 0
  }
]
  )
})


//
// Write your own mocking API endpoints here.
//

// Finally add your routes to the app
app.use(routes)

app.use('*', function (req, res, next) {
  next({ error: 'Non-existing endpoint' })
})

app.use((err, req, res, next) => {
  res.status(400).json(err)
})

app.listen(port, () => {
  console.log('Mock backend server running on port', port)
})
