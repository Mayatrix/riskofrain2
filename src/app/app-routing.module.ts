import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'

import { AuthGuard } from './auth/auth.guard'
import { AuthComponent } from './auth/auth.component'
import { CharactersComponent } from './characters/characters.component'
import { HomeComponent } from './home/home.component'
import { ItemsComponent } from './items/items.component'
import { CharacterCreateComponent } from './character-create/character-create.component'
import { ItemCreateComponent } from './item-create/item-create.component'
import { CharacterDetailComponent } from './characters/character-detail/character-detail.component'
import { AbilityCreateComponent } from './ability-create/ability-create.component'
import { AddItemComponent } from './add-item/add-item.component'
import { EnemiesComponent } from './enemies/enemies.component'
import { EnemyCreateComponent } from './enemy-create/enemy-create.component'
import { EnemyDetailComponent } from './enemies/enemy-detail/enemy-detail.component'
import { ItemDetailComponent } from './items/item-detail/item-detail.component'
import { UserdetailsComponent } from './userdetails/userdetails.component'

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '', component: HomeComponent },
  { path: 'characters', component: CharactersComponent },
  { path: 'enemies', component: EnemiesComponent },
  { path: 'additem', component: AddItemComponent, canActivate: [AuthGuard]  },
  { path: 'createcharacter', component: CharacterCreateComponent, canActivate: [AuthGuard] },
  { path: 'createability', component: AbilityCreateComponent, canActivate: [AuthGuard] },
  { path: 'createEnemy', component: EnemyCreateComponent, canActivate: [AuthGuard] },
  { path: 'characterdetails', component: CharacterDetailComponent, canActivate: [AuthGuard] },
  { path: 'userdetails', component: UserdetailsComponent, canActivate: [AuthGuard] },
  { path: 'enemydetails', component: EnemyDetailComponent, canActivate: [AuthGuard] },
  { path: 'itemdetails', component: ItemDetailComponent, canActivate: [AuthGuard] },
  { path: 'items', component: ItemsComponent },
  { path: 'createitem', component: ItemCreateComponent, canActivate: [AuthGuard] },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'about', component: UsecasesComponent },
  { path: 'auth', component: AuthComponent },
  { path: '**', redirectTo: 'home' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
