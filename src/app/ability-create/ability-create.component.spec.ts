import { ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { SharedModule } from '../core/Shared/shared.module'
import { ReactiveFormsModule, FormsModule, NgForm } from '@angular/forms'
import { AbilityCreateComponent } from './ability-create.component'

describe('AbilityCreateComponent', () => {

  let component: AbilityCreateComponent
  let fixture: ComponentFixture<AbilityCreateComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [AbilityCreateComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(AbilityCreateComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
