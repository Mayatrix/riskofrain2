import { Component, OnInit } from '@angular/core'
import { NgForm } from '@angular/forms'
import { AuthService } from '../../auth/auth.service'
import { CharacterService } from '../../services/character.service'
import { Router, ActivatedRoute } from '@angular/router'
import { ValueConverter } from '@angular/compiler/src/render3/view/template'
import { Character } from '../../models/Character'

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.scss']
})
export class CharacterDetailComponent implements OnInit {
  userId: string
  character: Character

  constructor(
    private authService: AuthService,
    private characterService: CharacterService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const characterId = this.route.snapshot.paramMap.get('characterId')

    if (characterId !== null) {
      this.characterService.getCharacter(characterId).subscribe((character) => {
        this.character = character
      })
    }
  }

  onBack() {
    this.router.navigate(['/characters'])
  }
}
