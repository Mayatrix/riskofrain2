import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from '../core/Shared/shared.module';
import { EnemyService } from '../services/enemy.service';

import { EnemiesComponent } from './enemies.component';

describe('EnemiesComponent', () => {
  let enemyService: EnemyService

  let httpSpy: any;
  let authServiceSpy: any;
  let enemyServiceSpy: any;

  let component: EnemiesComponent;
  let fixture: ComponentFixture<EnemiesComponent>;

  beforeEach(async () => {

    const Items = []

    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);

    enemyServiceSpy = jasmine.createSpyObj('ItemService', [
      'getEnemy',
      'getEnemioesByUserId',
      'getEnemies',
      'createEnemy',
      'updateEnemy',
      'deleteEnemy',
      'handleError'
    ]);

    enemyServiceSpy.getEnemies.and.returnValue([]);


    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'signup',
      'login',
      'autoLogin',
      'logout',
      'handleAuthentication'
    ]);

    enemyService = new  EnemyService(httpSpy, authServiceSpy)

    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [EnemiesComponent],
      providers: [{provide: EnemyService, useValue: enemyServiceSpy}]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnemiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
