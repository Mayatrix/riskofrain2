import { Ability } from './Ability'
import { Item } from './Item'

export class Character {
  _id: string
  name: string
  health: number
  userId
  speed: number
  basedamage: number
  abilities: Ability[]
  items: Item[]
}
