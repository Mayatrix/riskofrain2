import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker'
  readonly ADMIN_USER = 'Administrator'

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Registreren',
      description: 'Hiermee kan een gebruiker een account aanmaken.',
      scenario: [
        'Gebruiker vult email en password in en klikt op de registreer knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor heeft nog geen account',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-03',
      name: 'Aanpassen van gegevens als administrator',
      description:
        'Een administrator kan alle entiteiten in de applicatie aanpassen en verwijderen, dat geld dus ook voor entiteiten van andere gebruikers.',
      scenario: [
        'De gebruiker kiest een entiteit',
        'De gebruiker past de gegevens aan die hij of zij aan wil passen',
        'De applicatie valideert de gegevens en past de entiteit aan indien deze correct zijn ingevoerd'
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De gekozen entiteit is aangepast.'
    },
    {
      id: 'UC-04',
      name: 'Bekijken van entiteiten door sitebezoekers',
      description:
        'Een bezoeker van de site moet de verschillende entiteiten op de site kunnen bekijken, hiervoor hoeft de gebruiker niet ingelogd te zijn',
      scenario: ['De gebruiker bezoekt de site', 'De gebruiker bekijkt entiteiten op de site'],
      actor: this.PLAIN_USER,
      precondition: 'De gebruiker heeft een internetverbinding',
      postcondition: 'n.v.t'
    },
    {
      id: 'UC-05',
      name: 'Maken van entiteiten',
      description:
        'Een ingelogde gebruiker kan entiteiten maken en eniteiten die door deze gebruiker aangemaakt zijn kunnen ook aangepast worden',
      scenario: [
        'De gebruiker kiest een entiteit en vult hiervoor de gegevens in',
        'De applicatie valideert de gegevens en past/maakt de entiteit aan indien deze correct zijn ingevoerd',
        'De gebruiker bekijkt entiteiten op de site'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De gebruiker is ingelogd',
      postcondition: 'n.v.t'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
