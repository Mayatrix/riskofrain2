import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpClientModule, HttpErrorResponse } from '@angular/common/http'
import { Item } from '../models/Item'
import { Ability } from '../models/Ability'
import { catchError, retry, take, exhaustMap } from 'rxjs/operators'
import { Observable, throwError, empty } from 'rxjs'
import { AuthService } from '../auth/auth.service'
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  BASEURL = environment.apiUrl
  // BASEURL = 'https://ror2api.herokuapp.com/api'
  ITEMURL = '/items'
  USERURL = '/users'

  constructor(private http: HttpClient, private authService: AuthService) {}

  getItem(itemId): Observable<Item> {
    return this.http
      .get<Item>(`${this.BASEURL + this.ITEMURL + '/' + itemId}`)
      .pipe(catchError(this.handleError))
  }

  getItemsByUserId(userId): Observable<Item[]> {
    return this.http
      .get<Item[]>(`${this.BASEURL + this.ITEMURL + this.USERURL + '/' + userId}`)
      .pipe(catchError(this.handleError))
  }

  getItems(): Observable<Item[]> {
    return this.http.get<Item[]>(`${this.BASEURL + this.ITEMURL}`).pipe(catchError(this.handleError))
  }

  createItem(ItemParams) {
    return this.http
      .post<Item>(`${this.BASEURL + this.ITEMURL}` + '/create', ItemParams)
      .pipe(catchError(this.handleError))
  }

  updateItem(itemId, ItemParams) {
    return this.http
      .put<Item>(`${this.BASEURL + this.ITEMURL + '/' + itemId}`, ItemParams)
      .pipe(catchError(this.handleError))
  }

  deleteITem(itemId) {
    return this.http
      .delete<Item>(`${this.BASEURL + this.ITEMURL + '/' + itemId}`, {})
      .pipe(catchError(this.handleError))
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message)
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`)
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later!')
  }
}
