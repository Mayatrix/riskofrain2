import { Component, Input, OnInit } from '@angular/core'
import { Enemy } from '../../models/Enemy'
import { AuthService } from '../../auth/auth.service'
import { EnemyService } from '../../services/enemy.service'
import { Router } from '@angular/router'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-enemy-card',
  templateUrl: './enemy-card.component.html',
  styleUrls: ['./enemy-card.component.scss']
})
export class EnemyCardComponent implements OnInit {
  @Input() enemy: Enemy
  userId: string
  userEmail: string
  destroyed = false
  public isAuthenticated = false
  private userSubscription: Subscription

  constructor(private authService: AuthService, private enemyService: EnemyService, private router: Router) {}

  ngOnInit() {
    this.authService.user.subscribe((user) => {
      this.userId = user === null ? '' : user._id
      this.userEmail = user === null ? '' : user.email
    })
    {
    this.userSubscription = this.authService.user.subscribe((user) => {
      this.isAuthenticated = !!user
    })
  }
  }
  onDelete(enemyId: string) {
    this.destroyed = true
    this.enemyService.deleteEnemy(enemyId).subscribe((character) => {})
  }

  onEdit(enemyId: string) {
    this.router.navigate(['/createEnemy', { enemyId }])
  }

  onDetails(enemyId: string) {
    this.router.navigate(['/enemydetails', { enemyId }])
  }
}
