import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from 'src/app/core/Shared/shared.module';

import { EnemyDetailComponent } from './enemy-detail.component';

describe('EnemyDetailComponent', () => {
  let component: EnemyDetailComponent;
  let fixture: ComponentFixture<EnemyDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [ EnemyDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnemyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
