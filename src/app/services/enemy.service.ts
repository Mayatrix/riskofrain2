import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule, HttpErrorResponse } from '@angular/common/http'
import { Enemy } from '../models/Enemy'
import { Ability } from '../models/Ability'
import { catchError, retry, take, exhaustMap } from 'rxjs/operators'
import { Observable, throwError, empty } from 'rxjs'
import { AuthService } from '../auth/auth.service'
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class EnemyService {
  BASEURL = environment.apiUrl
  // BASEURL = 'https://ror2api.herokuapp.com/api'
  ENEMYURL = '/enemies'
  USERURL = '/users'

  constructor(private http: HttpClient, private authService: AuthService) {}

  getEnemy(enemyId): Observable<Enemy> {
    return this.http
      .get<Enemy>(`${this.BASEURL + this.ENEMYURL + '/' + enemyId}`)
      .pipe(catchError(this.handleError))
  }

  getEnemiesByUserId(userId): Observable<Enemy[]> {
    return this.http
      .get<Enemy[]>(`${this.BASEURL + this.ENEMYURL + this.USERURL + '/' + userId}`)
      .pipe(catchError(this.handleError))
  }

  getEnemies(): Observable<Enemy[]> {
    return this.http.get<Enemy[]>(`${this.BASEURL + this.ENEMYURL}`).pipe(catchError(this.handleError))
  }

  createEnemy(EnemyParams) {
    return this.http
      .post<Enemy>(`${this.BASEURL + this.ENEMYURL}` + '/create', EnemyParams)
      .pipe(catchError(this.handleError))
  }

  updateEnemy(enemyId, EnemyParams) {
    return this.http
      .put<Enemy>(`${this.BASEURL + this.ENEMYURL + '/' + enemyId}`, EnemyParams)
      .pipe(catchError(this.handleError))
  }

  deleteEnemy(enemyId) {
    return this.http
      .delete<Enemy>(`${this.BASEURL + this.ENEMYURL + '/' + enemyId}`, {})
      .pipe(catchError(this.handleError))
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message)
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`)
    }
    // return an observable with a user-facing error message
    return throwError(error)
  }
}
