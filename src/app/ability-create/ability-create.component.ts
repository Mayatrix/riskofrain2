import { Component, OnInit } from '@angular/core'
import { NgForm, FormGroup, Validators, FormControl, EmailValidator } from '@angular/forms'
import { AuthService } from '../auth/auth.service'
import { Router, ActivatedRoute } from '@angular/router'
import { ValueConverter } from '@angular/compiler/src/render3/view/template'
import { Ability } from '../models/Ability'
import { CharacterService } from '../services/character.service'

@Component({
  selector: 'app-ability-create',
  templateUrl: './ability-create.component.html',
  styleUrls: ['./ability-create.component.scss']
})
export class AbilityCreateComponent implements OnInit {
  userId: string
  abilityId: string
  characterId: string
  newAbility = true

  name: string
  description: string
  damage: number
  cooldown: number

  theAbility: Ability

  headerTitle: string

  myEpicForm = new FormGroup({
    abilityName: new FormControl('', [Validators.required, Validators.maxLength(20)]),
    abilityDescription: new FormControl('', Validators.required),
    abilityDamage: new FormControl('', [Validators.required, Validators.max(250), Validators.min(1)]),
    abilityCooldown: new FormControl('', [Validators.required, Validators.max(60), , Validators.min(1)])
  })

  constructor(
    private authService: AuthService,
    private characterService: CharacterService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.characterId = this.route.snapshot.paramMap.get('characterId')
    this.abilityId = this.route.snapshot.paramMap.get('abilityId')

    if (this.abilityId !== null) {
      this.newAbility = false

      this.characterService.getCharacter(this.characterId).subscribe((character) => {
        this.theAbility = character.abilities.find((abilities) => abilities._id === this.abilityId)

        this.myEpicForm.setValue({
          abilityName: this.theAbility.name,
          abilityDescription: this.theAbility.description,
          abilityDamage: this.theAbility.damage,
          abilityCooldown: this.theAbility.cooldown
        })
      })
      this.headerTitle = 'Update Ability'
    } else {
      this.headerTitle = 'Create Ability'
    }

    this.authService.user.subscribe((user) => {
      this.userId = user === null ? '' : user._id
    })
  }

  onSaveAbility(form: FormGroup) {
    const value = form.value

    if (this.newAbility) {
      const abilityParams = {
        name: value.abilityName,
        description: value.abilityDescription,
        damage: value.abilityDamage,
        cooldown: value.abilityCooldown,
        userId: this.userId
      }
      this.characterService.createAbility(this.characterId, abilityParams).subscribe((ability) => {
        this.router.navigate(['/characters'])
      })
    } else {
      const abilityParams = {
        name: value.abilityName,
        description: value.abilityDescription,
        damage: value.abilityDamage,
        cooldown: value.abilityCooldown
      }

      this.characterService
        .updateAbility(this.characterId, this.abilityId, abilityParams)
        .subscribe((ability) => {
          this.router.navigate(['/characters'])
        })
    }
  }
}
