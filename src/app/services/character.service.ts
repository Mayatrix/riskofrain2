import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpClientModule, HttpErrorResponse } from '@angular/common/http'
import { Character } from '../models/Character'
import { Ability } from '../models/Ability'
import { catchError, retry, take, exhaustMap } from 'rxjs/operators'
import { Observable, throwError, empty } from 'rxjs'
import { AuthService } from '../auth/auth.service'
import { environment } from '../../environments/environment'
import { Item } from '../models/Item'

@Injectable({
  providedIn: 'root'
})
export class CharacterService {
  BASEURL = environment.apiUrl
  // BASEURL = 'https://ror2api.herokuapp.com/api'

  CHARACTERURL = '/characters'
  ABILITYURL = '/abilities'
  ITEMURL = '/items'
  USERURL = '/users'

  constructor(private http: HttpClient, private authService: AuthService) {}

  getCharacter(characterId): Observable<Character> {
    return this.http
      .get<Character>(`${this.BASEURL + this.CHARACTERURL + '/' + characterId}`)
      .pipe(catchError(this.handleError))
  }

  getCharacters(): Observable<Character[]> {
    return this.http
      .get<Character[]>(`${this.BASEURL + this.CHARACTERURL}`)
      .pipe(catchError(this.handleError))
  }

  getCharactersByUserId(userId): Observable<Character[]> {
    return this.http
      .get<Character[]>(`${this.BASEURL + this.CHARACTERURL + this.USERURL + '/' + userId}`)
      .pipe(catchError(this.handleError))
  }

  createCharacter(characterParams) {
    return this.http
      .post<Character>(`${this.BASEURL + this.CHARACTERURL}` + '/create', characterParams)
      .pipe(catchError(this.handleError))
  }

  updateCharacter(characterId, characterParams): Observable<Character> {
    return this.http
      .put<Character>(`${this.BASEURL + this.CHARACTERURL + '/' + characterId}`, characterParams)
      .pipe(catchError(this.handleError))
  }

  deleteCharacter(characterId) {
    return this.http
      .delete<Character>(`${this.BASEURL + this.CHARACTERURL + '/' + characterId}`, {})
      .pipe(catchError(this.handleError))
  }

  createAbility(characterId, abilityParams) {
    return this.http
      .post<Ability>(
        `${this.BASEURL + this.CHARACTERURL + '/' + characterId + this.ABILITYURL + '/'}`,
        abilityParams
      )
      .pipe(catchError(this.handleError))
  }

  updateAbility(characterId, abilityId, abilityParams): Observable<Ability> {
    return this.http
      .put<Ability>(
        `${this.BASEURL + this.CHARACTERURL + '/' + characterId + this.ABILITYURL + '/' + abilityId}`,
        abilityParams
      )
      .pipe(catchError(this.handleError))
  }

  deleteAbility(characterId, abilityId) {
    return this.http
      .delete<Ability>(
        `${
          this.BASEURL + this.CHARACTERURL + '/' + characterId + this.ABILITYURL + '/' + abilityId + '/delete'
        }`,
        {}
      )
      .pipe(catchError(this.handleError))
  }

  addItemToCharacter(characterId, itemId) {
    return this.http
      .post<Item>(
        `${
          this.BASEURL + this.CHARACTERURL + '/' + characterId + this.ITEMURL + '/' + itemId
        }`,
        {}
      )
      .pipe(catchError(this.handleError))
  }

  deleteItemFromCharacter(characterId, itemId) {
    return this.http
      .delete<Item>(
        `${
          this.BASEURL + this.CHARACTERURL + '/' + characterId  + this.ITEMURL + '/' + itemId + '/delete'
        }`,
        {}
      )
      .pipe(catchError(this.handleError))
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message)
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`)
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later!')
  }
}
