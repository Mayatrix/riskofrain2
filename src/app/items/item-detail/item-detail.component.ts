import { Component, OnInit } from '@angular/core'
import { NgForm } from '@angular/forms'
import { AuthService } from '../../auth/auth.service'
import { Router, ActivatedRoute } from '@angular/router'
import { ValueConverter } from '@angular/compiler/src/render3/view/template'
import { Item } from 'src/app/models/Item';
import { ItemService } from 'src/app/services/item.service'

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.scss']
})
export class ItemDetailComponent implements OnInit {
  item: Item

  constructor
  (private authService: AuthService,
    private itemService: ItemService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    const itemId = this.route.snapshot.paramMap.get('itemId')

    if (itemId !== null) {
      this.itemService.getItem(itemId).subscribe((item) => {
        this.item = item
      })
    }
  }

  onBack() {
    this.router.navigate(['/items'])
  }

}
