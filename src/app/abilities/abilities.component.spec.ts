import { ComponentFixture, TestBed } from '@angular/core/testing'
import { SharedModule } from '../core/Shared/shared.module'

import { AbilitiesComponent } from './abilities.component'

describe('AbilitiesComponent', () => {
  let component: AbilitiesComponent
  let fixture: ComponentFixture<AbilitiesComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [AbilitiesComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(AbilitiesComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
