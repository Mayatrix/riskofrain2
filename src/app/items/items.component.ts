import { Component, OnInit } from '@angular/core'
import { Item } from '../models/Item'
import { ItemService } from '../services/item.service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { Observable, Subscription } from 'rxjs'
import { interval } from 'rxjs'
import { mapTo, startWith, map, flatMap } from 'rxjs/operators'
import { AuthService } from '../auth/auth.service'
import { Ability } from '../models/Ability'

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {
  items: Observable<Item[]>
  refreshInterval = 4500
  isLoading = true
  public isAuthenticated = false
  private userSubscription: Subscription

  constructor(private itemService: ItemService, private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.items = interval(this.refreshInterval).pipe(
      startWith(0),
      mapTo((this.items = this.itemService.getItems())),
      flatMap((v) => v)
    )

    if (this.items !== undefined) {
      this.items.subscribe(() => {
        this.isLoading = false
      })
    }

    {
      this.userSubscription = this.authService.user.subscribe((user) => {
        this.isAuthenticated = !!user
      })
    }
  }
}
