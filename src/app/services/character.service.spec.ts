import { TestBed } from '@angular/core/testing'
import { SharedModule } from '../core/Shared/shared.module'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpClientModule, HttpErrorResponse } from '@angular/common/http'
import { Character } from '../models/Character'
import { Ability } from '../models/Ability'
import { catchError, retry, take, exhaustMap } from 'rxjs/operators'
import { Observable, throwError, empty } from 'rxjs'
import { AuthService } from '../auth/auth.service'
import { environment } from '../../environments/environment'

import { CharacterService } from './character.service'
import { HttpTestingController } from '@angular/common/http/testing'

describe('CharacterService', () => {
  let service: CharacterService
  let httpMock: HttpTestingController

  const BASEURL = environment.apiUrl
  // const BASEURL = 'https://ror2api.herokuapp.com/api'

  const CHARACTERURL = '/characters'
  const ABILITYURL = '/abilities'

  const dummyCharactersResponse: Character[] = [
    {
      _id: '1',
      name: 'aArrayCharacterName',
      userId: 1,
      health: 100,
      speed: 1,
      basedamage: 120,
      abilities: [],
      items: []
    },
    {
      _id: '2',
      name: 'AnotherArrayCharacterName',
      userId: 2,
      health: 100,
      speed: 1,
      basedamage: 120,
      abilities: [],
      items: []
    }
  ]

  const dummyCharacterResponse: Character = {
    _id: '3',
    name: 'aCharacterName',
    userId: 1,
    health: 100,
    speed: 1,
    basedamage: 120,
    abilities: [],
    items: []
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, HttpClientTestingModule],
      providers: [CharacterService]
    })
    service = TestBed.inject(CharacterService)
    httpMock = TestBed.inject(HttpTestingController)
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  it('should retrieve all characters', () => {
    service.getCharacters().subscribe((res) => {
      expect(res).toEqual(dummyCharactersResponse)
    })

    const req = httpMock.expectOne(BASEURL + CHARACTERURL)
    expect(req.request.method).toBe('GET')
    req.flush(dummyCharactersResponse)
  })

  it('should retrieve one character', () => {
    service.getCharacter(dummyCharacterResponse._id).subscribe((res) => {
      expect(res).toEqual(dummyCharacterResponse)
      console.log('The current environment API URL is: ' + environment.apiUrl)
    })

    const req = httpMock.expectOne(BASEURL + CHARACTERURL + '/' + dummyCharacterResponse._id)
    expect(req.request.method).toBe('GET')
    req.flush(dummyCharacterResponse)
  })

  it('should create a new character', () => {
    service.createCharacter(dummyCharacterResponse).subscribe((res) => {
      expect(res).toEqual(dummyCharacterResponse)
    })

    const req = httpMock.expectOne(BASEURL + CHARACTERURL + '/create')
    expect(req.request.method).toBe('POST')
    req.flush(dummyCharacterResponse)
  })
})
