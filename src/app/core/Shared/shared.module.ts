import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatButtonModule } from '@angular/material/button'
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatIconModule } from '@angular/material/icon'
import { MatTableModule } from '@angular/material/table'
import { MatListModule } from '@angular/material/list'
import { MatCardModule } from '@angular/material/card'
import { MatCheckboxModule } from '@angular/material/checkbox'
import { MatOptionModule } from '@angular/material/core/option'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { MatSelectModule } from '@angular/material/select'
import { HttpClientModule } from '@angular/common/http'
import { MatTreeModule } from '@angular/material/tree'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatTabsModule } from '@angular/material/tabs'
import { MatMenuModule } from '@angular/material/menu'
import { MatDialogModule } from '@angular/material/dialog'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { AngularEditorModule } from '@kolkov/angular-editor'
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component'
import { MatProgressBarModule } from '@angular/material/progress-bar'

@NgModule({
  imports: [
    AngularEditorModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatCheckboxModule,
    HttpClientModule,
    MatTreeModule,
    FormsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([]),
    MatTableModule,
    MatTabsModule,
    NgbModule,
    MatMenuModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatProgressBarModule
  ],
  providers: [],
  declarations: [LoadingSpinnerComponent],
  exports: [
    AngularEditorModule,
    ReactiveFormsModule,
    RouterModule,
    LoadingSpinnerComponent,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatCheckboxModule,
    HttpClientModule,
    MatTreeModule,
    FormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatTabsModule,
    NgbModule,
    MatMenuModule,
    MatProgressBarModule,
    MatDialogModule
  ]
})
export class SharedModule {}
