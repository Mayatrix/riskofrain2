import { ComponentFixture, TestBed } from '@angular/core/testing'
import { Observable } from 'rxjs'
import { SharedModule } from '../core/Shared/shared.module'
import { CharacterService } from '../services/character.service'

import { CharactersComponent } from './characters.component'

describe('CharactersComponent', () => {
  let component: CharactersComponent
  let fixture: ComponentFixture<CharactersComponent>
  let chararcterServiceSpy: any;

  beforeEach(async () => {

    chararcterServiceSpy = jasmine.createSpyObj('CharacterService', [
      'getCharacter',
      'getCharacters',
      'createCharacter',
      'updateCharacter',
      'deleteCharacter',
      'createAbility',
      'updateAbility',
      'deleteAbility',
      'addItemToCharacter',
      'deleteItemFromCharacter',
      'handleError'
    ]);

    chararcterServiceSpy.getCharacters.and.returnValue(new Observable<any>());

    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [CharactersComponent],
      providers: [{provide: CharacterService, useValue: chararcterServiceSpy}]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(CharactersComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should have the loading component', () => {
    const compiled = fixture.debugElement.nativeElement
    expect(compiled.querySelector('app-loading-spinner')).not.toBe(null)
  })
})
