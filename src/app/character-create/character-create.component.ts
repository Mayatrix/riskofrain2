import { Component, OnInit } from '@angular/core'
import { NgForm, FormGroup, Validators, FormControl, EmailValidator } from '@angular/forms'
import { AuthService } from '../auth/auth.service'
import { CharacterService } from '../services/character.service'
import { Router, ActivatedRoute } from '@angular/router'
import { ValueConverter } from '@angular/compiler/src/render3/view/template'
import { Character } from '../models/Character'

@Component({
  selector: 'app-character-create',
  templateUrl: './character-create.component.html',
  styleUrls: ['./character-create.component.scss']
})
export class CharacterCreateComponent implements OnInit {
  userId: string
  newCharacter = true

  formName: string
  formHealth: number
  formDamage: number
  formSpeed: number
  headerTitle: string
  characterId: string

  myEpicForm = new FormGroup({
    characterName: new FormControl('', [Validators.required, Validators.maxLength(10)]),
    characterHealth: new FormControl('', [Validators.required, Validators.max(500), Validators.min(1)]),
    characterDamage: new FormControl('', [Validators.required, Validators.max(120), Validators.min(1)]),
    characterSpeed: new FormControl('', [Validators.required, Validators.max(2.5), , Validators.min(0.01)])
  })

  constructor(
    private authService: AuthService,
    private characterService: CharacterService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const characterId = this.route.snapshot.paramMap.get('characterId')

    if (characterId !== null) {
      this.newCharacter = false

      this.characterService.getCharacter(characterId).subscribe((character) => {
        this.formName = character.name
        this.formHealth = character.health
        this.formDamage = character.basedamage
        this.formSpeed = character.speed
        this.characterId = character._id

        this.myEpicForm.setValue({
          characterName: character.name,
          characterHealth: character.health,
          characterDamage: character.basedamage,
          characterSpeed: character.speed
        })
      })
      this.headerTitle = 'Update Character'
    } else {
      this.headerTitle = 'Create Character'
    }

    this.authService.user.subscribe((user) => {
      this.userId = user === null ? '' : user._id
    })
  }

  onSaveCharacter(form: FormGroup) {
    const value = form.value

    if (this.newCharacter) {
      const characterParams = {
        name: value.characterName,
        health: value.characterHealth,
        basedamage: value.characterDamage,
        speed: value.characterSpeed,
        userId: this.userId
      }
      this.characterService.createCharacter(characterParams).subscribe((character) => {
        this.router.navigate(['/characters'])
      })
    } else {
      const characterParams = {
        name: value.characterName,
        health: value.characterHealth,
        basedamage: value.characterDamage,
        speed: value.characterSpeed
      }

      this.characterService.updateCharacter(this.characterId, characterParams).subscribe((character) => {
        this.router.navigate(['/characters'])
      })
    }
  }
}
