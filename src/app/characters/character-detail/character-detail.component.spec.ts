import { ComponentFixture, TestBed } from '@angular/core/testing'
import { SharedModule } from 'src/app/core/Shared/shared.module'

import { CharacterDetailComponent } from './character-detail.component'

describe('CharacterDetailComponent', () => {
  let component: CharacterDetailComponent
  let fixture: ComponentFixture<CharacterDetailComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [CharacterDetailComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterDetailComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
