import { ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { SharedModule } from '../core/Shared/shared.module'
import { ReactiveFormsModule, FormsModule, NgForm, MaxLengthValidator, AbstractControl } from '@angular/forms'
import { CharacterCreateComponent } from './character-create.component'
import { kMaxLength, kStringMaxLength } from 'buffer'

const thisName = 'characterName'
const thisDamage = 'characterDamage'
const thisHealth = 'characterHealth'
const thisSpeed = 'characterSpeed'

describe('CharacterCreateComponent', () => {
  let component: CharacterCreateComponent
  let fixture: ComponentFixture<CharacterCreateComponent>

  afterAll(() => {
    TestBed.resetTestingModule()
  })

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [CharacterCreateComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterCreateComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('Name should be false if it exceeds 10 characters', () => {
    const form = component.myEpicForm
    const name: AbstractControl = form.get(thisName)

    name.setValue('aReallyReallyLongName')
    console.log(name.errors)
    expect(name.hasError('maxlength')).toBeTruthy()
    expect(form.valid).toBeFalsy()
  })

  it('Health should be false if it exceeds 500', () => {
    const form = component.myEpicForm
    const health: AbstractControl = form.get(thisHealth)

    health.setValue(501)
    console.log(health.errors)
    expect(health.hasError('max')).toBeTruthy()
    expect(form.valid).toBeFalsy()
  })

  it('Damage should be false if it exceeds 120', () => {
    const form = component.myEpicForm
    const damage: AbstractControl = form.get(thisDamage)

    damage.setValue(121)
    console.log(damage.errors)
    expect(damage.hasError('max')).toBeTruthy()
    expect(form.valid).toBeFalsy()
  })

  it('Speed should be false if it exceeds 2.5', () => {
    const form = component.myEpicForm
    const speed: AbstractControl = form.get(thisSpeed)

    speed.setValue(2.6)
    console.log(speed.errors)
    expect(speed.hasError('max')).toBeTruthy()
    expect(form.valid).toBeFalsy()
  })

  it('Damage can not be a negative value', () => {
    const form = component.myEpicForm
    const damage: AbstractControl = form.get(thisDamage)

    damage.setValue(-3)
    console.log(damage.errors)
    expect(damage.hasError('min')).toBeTruthy()
    expect(form.valid).toBeFalsy()
  })

  it('the form should return true for a category if that category is filled in', () => {
    const name = component.myEpicForm.controls[thisName]
    name.setValue('anyName')
    expect(name.valid).toBeTruthy()
  })

  it('the form should return false if the form is not completely filled in', () => {
    const name = component.myEpicForm.controls[thisName]
    name.setValue('anyName')
    expect(component.myEpicForm.valid).toBeFalsy()
  })

  it('the form should return true if the form is completely filled in', () => {
    const name = component.myEpicForm.controls[thisName]
    const damage = component.myEpicForm.controls[thisDamage]
    const health = component.myEpicForm.controls[thisHealth]
    const speed = component.myEpicForm.controls[thisSpeed]
    name.setValue('anyName')
    health.setValue(100)
    damage.setValue(10)
    speed.setValue(1)
    expect(component.myEpicForm.valid).toBeTruthy()
  })
})
