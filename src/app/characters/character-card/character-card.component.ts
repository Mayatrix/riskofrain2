import { Component, Input, OnInit } from '@angular/core'
import { Character } from '../../models/Character'
import { AuthService } from '../../auth/auth.service'
import { Observable, Subscription } from 'rxjs'
import { CharacterService } from '../../services/character.service'
import { Router } from '@angular/router'
import { ItemService } from 'src/app/services/item.service'

@Component({
  selector: 'app-character-card',
  templateUrl: './character-card.component.html',
  styleUrls: ['./character-card.component.scss']
})
export class CharacterCardComponent implements OnInit {
  @Input() character: Character
  userId: string
  userEmail: string
  destroyed = false
  abilityDestroyed = false
  itemDestroyed = false
  public isAuthenticated = false
  private userSubscription: Subscription


  constructor(
    private authService: AuthService,
    private characterService: CharacterService,
    private itemservice: ItemService,
    private router: Router
  ) {}

  ngOnInit() {
    this.authService.user.subscribe((user) => {
      this.userId = user === null ? '' : user._id
      this.userEmail = user === null ? '' : user.email
    })

    {
      this.userSubscription = this.authService.user.subscribe((user) => {
        this.isAuthenticated = !!user
      })
    }
  }
  onDelete(characterId: string) {
    this.destroyed = true
    this.characterService.deleteCharacter(characterId).subscribe((character) => {})
  }

  onEdit(characterId: string) {
    this.router.navigate(['/createcharacter', { characterId }])
  }
  onDetails(characterId: string) {
    this.router.navigate(['/characterdetails', { characterId }])
  }

  onDeleteAbility(characterId: string, abilityId: string) {
    this.abilityDestroyed = true
    this.characterService.deleteAbility(characterId, abilityId).subscribe((ability) => {})
  }

  onDeleteItem(characterId: string, itemId: string) {
    this.itemDestroyed = true
    this.characterService.deleteItemFromCharacter(characterId, itemId).subscribe((item) => {})
  }

  onEditItem(itemId: string) {
    this.router.navigate(['/createitem', { itemId }])
  }

  onEditAbility(characterId: string, abilityId: string) {
    this.router.navigate(['/createability', { characterId, abilityId }])
  }

  onCreateAbility(characterId: string) {
    this.router.navigate(['/createability', { characterId }])
  }

  onAddItem(characterId: string) {
    this.router.navigate(['/additem', { characterId }])
  }

  onCreateItem(characterId: string) {
    this.router.navigate(['/createitem', { characterId }])
  }

  onDeleteItemCompletely(itemId: string) {
    this.itemDestroyed = true
    this.itemservice.deleteITem(itemId).subscribe((character) => {})
  }
}
