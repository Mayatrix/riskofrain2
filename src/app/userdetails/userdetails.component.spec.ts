import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { SharedModule } from '../core/Shared/shared.module';
import { CharacterService } from '../services/character.service';
import { EnemyService } from '../services/enemy.service';
import { ItemService } from '../services/item.service';

import { UserdetailsComponent } from './userdetails.component';

describe('UserdetailsComponent', () => {
  let component: UserdetailsComponent;
  let fixture: ComponentFixture<UserdetailsComponent>;
  let chararcterServiceSpy: any;
  let enemyServiceSpy: any;
  let itemsServiceSpy: any;


  beforeEach(async () => {

    chararcterServiceSpy = jasmine.createSpyObj('CharacterService', [
      'getCharacter',
      'getCharacters',
      'getCharactersByUserId',
      'createCharacter',
      'updateCharacter',
      'deleteCharacter',
      'createAbility',
      'updateAbility',
      'deleteAbility',
      'addItemToCharacter',
      'deleteItemFromCharacter',
      'handleError'
    ]);

    enemyServiceSpy = jasmine.createSpyObj('EnemyService', [
      'getEnemiesByUserId'
    ]);

    itemsServiceSpy = jasmine.createSpyObj('ItemService', [
      'getItemsByUserId'
    ]);


    chararcterServiceSpy.getCharactersByUserId.and.returnValue(new Observable<any>());
    enemyServiceSpy.getEnemiesByUserId.and.returnValue(new Observable<any>());
    itemsServiceSpy.getItemsByUserId.and.returnValue(new Observable<any>());


    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [ UserdetailsComponent ],
      providers: [{provide: CharacterService, useValue: chararcterServiceSpy},
      {provide: ItemService, useValue: itemsServiceSpy},
      {provide: EnemyService, useValue: enemyServiceSpy}]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
